__author__ = 'Tejal'
# Converts the input xls files into separate csv files for each sheet of the xls files

import xlrd
import csv
import os
import win32com.client as win32

# input directory where xls files are located
rootdir = 'C:\\Users\\Tejal\\Documents\\NACC\\UDS2Migration\\inputall\\UDS2-1'

# output directory where xls files are located
outputdir = 'C:\\Users\\Tejal\\Documents\\NACC\\UDS2Migration\\output1'

# writes the mysql import commands to this file
# f = open(outputdir+'\\script\\'+"script.txt","w")
# writes the drop table commands to this file
# f2 = open(outputdir+'\\script\\'+"drop_table_script.txt","w")

scriptString = "mysqlimport --local --delete --fields-terminated-by=, --lines-terminated-by='\\n' --fields-optionally-enclosed-by='\"' --ignore-lines=1 -p -u root uds_test "

for subdir, dirs, files in os.walk(rootdir):
    # iterates over all files in the directory
    for file in files:
        # coverts the xls to xlsx because NACC xls file format is different
        fname = rootdir+"\\"+file
        excel = win32.gencache.EnsureDispatch('Excel.Application')
        wb = excel.Workbooks.Open(fname)

        wb.SaveAs(fname+"x", FileFormat = 51)    #FileFormat = 51 is for .xlsx extension
        wb.Close()                               #FileFormat = 56 is for .xls extension
        excel.Application.Quit()

        # open the created xlsx file
        filename_without_ext = file.split(".")
        wb = xlrd.open_workbook(rootdir+"\\"+filename_without_ext[0]+".xlsx")
        # iterate over each sheet of the xlsx file
        for sh in wb.sheets():
            # print(sh.cell(1,3).value)
            # open csv file and name it as per the form name obtained from 1,3 cell
            your_csv_file = open(outputdir+'\\csv\\'+sh.cell(1,3).value.lower()+'_uds_v2.csv', 'wb')
            # write data to the csv
            wr = csv.writer(your_csv_file, quoting=csv.QUOTE_NONNUMERIC)
            for rownum in xrange(sh.nrows):
                # print(sh.row_values(rownum))
                wr.writerow(sh.row_values(rownum))
            your_csv_file.close()

            # write mysql import command to file for table corresponding this form
            # f.write(scriptString+sh.cell(1,3).value.lower()+'_uds_v2.csv'+'\n\n')
            # write drop table command to file for table corresponding this form
            # f2.write("DROP TABLE IF EXISTS '"+sh.cell(1,3).value.lower()+"_uds_v2';\n")

        # sh = wb.sheet_by_index(0)
