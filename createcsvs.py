__author__ = 'Tejal'

import xlrd
import csv

# input directory where xls files are located
rootdir = 'C:\\Users\\Tejal\\Documents\\NACC\\UDS2Migration\\inputall\\UDS2-1'

# output directory where xls files are located
outputdir = 'C:\\Users\\Tejal\\Documents\\NACC\\UDS2Migration\\output1'


wb = xlrd.open_workbook(rootdir+"\\"+"UDS2-1.xlsx")
# iterate over each sheet of the xlsx file
for sh in wb.sheets():
    # print(sh.cell(1,3).value)
    # open csv file and name it as per the form name obtained from 1,3 cell
    your_csv_file = open(outputdir+'\\csv\\'+sh.cell(1,3).value.lower()+'_uds_v2.csv', 'wb')
    # write data to the csv
    wr = csv.writer(your_csv_file, quoting=csv.QUOTE_NONNUMERIC)
    for rownum in xrange(sh.nrows):
        # print(sh.row_values(rownum))
        wr.writerow(sh.row_values(rownum))
    your_csv_file.close()