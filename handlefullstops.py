__author__ = 'Tejal'
# Removes decimal points from whole numbers
# Removes full stops put by NACC to denote null values
import os
import re

# input directory where csv files are located
inputdir = 'C:\\Users\\Tejal\\Documents\\NACC\\UDS2Migration\\output1\\csv'

for subdir, dirs, files in os.walk(inputdir):
    # iterates over all files in the directory
    for file in files:
        fname = inputdir+"\\"+file

        lines = []

        # open the file
        f = open(fname,"r")
        # read file line by line
        while True:
            # print("line")
            line = f.readline()
            if not line:
                break
            else:
                # remove decimal points from whole numbers
                line = re.sub('\.0,', ',', line)
                line = re.sub('\.0\\n', '\\n', line)
                # substitute full stop with null
                line = re.sub('"\."', 'NULL', line) #when you want field to be blank instead of 0
                # substitute full stop with 0
                # this line should be uncommented and previous one should commented out
                # to generate create table statements using mysql workbench
                # setting value as 0 ensures column type is taken as int
                # this line should be left as a comment otherwise
                # line = re.sub('"\."', '0', line)
                words = line.split(",")
                # print(words)
                # the NACC xls files have one blank line at the end which needs to be removed
                allblanks = True
                for word in words:
                    if word != '\"\"' and word != '\"\"\n':
                        allblanks = False
                        break
                    else:
                        print(word)
                if allblanks == False:
                    lines.append(line)
        f.close()

        # write all lines back to the csv
        f = open(fname,"w")
        for line in lines:
            # print(line)
            f.write(line)
        f.close()