#!/bin/bash

mysql --user=root --password uds_new < sql/drop_create_script.sql          

cd csv

mysqlimport --local --delete --fields-terminated-by=, --lines-terminated-by='\r\n' --fields-optionally-enclosed-by='"' --ignore-lines=1 -p -u root uds_new *.csv

mysql --user=root --password uds_new < ../sql/insert_new_ptid.sql

mysql --user=root --password uds_new < ../sql/add_all_fk.sql