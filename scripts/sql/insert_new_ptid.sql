INSERT INTO `PatientID` (`ptid`,`enrollment`,`cohort`)
   SELECT DISTINCT ptid, NULL, NULL
   FROM a1_uds_v2
   WHERE a1_uds_v2.ptid NOT IN (SELECT ptid FROM PatientID);