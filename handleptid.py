__author__ = 'Tejal'
# Removes decimal points from whole numbers
# Removes full stops put by NACC to denote null values
import os
import csv

# input directory where csv files are located
inputdir = 'C:\\Users\\Tejal\\Documents\\NACC\\UDS2Migration\\output1\\csv'

for subdir, dirs, files in os.walk(inputdir):
    # iterates over all files in the directory
    for file in files:
        fname = inputdir+"\\"+file

        lines = []

        # open the file
        f1 = open(fname,"r")
        reader = csv.reader(f1)
        for row in reader:
            if row[1] != 'ptid' or row[1] != 'PTID':
                row[1] = '0'+str(row[1])
            print row[1]
            lines.append(row)
        f1.close()

        f2 = open(fname,"wb")
        writer = csv.writer(f2)
        for line in lines:
            print(line)
            writer.writerow(line)
        f2.close()